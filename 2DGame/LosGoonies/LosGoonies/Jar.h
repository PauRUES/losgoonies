#ifndef _JAR_INCLUDE
#define _JAR_INCLUDE

#include <json.hpp>
#include "Sprite.h"

using json = nlohmann::json;

class Jar {

public:
	Jar();
	void init(const glm::ivec2& tileMapPos, ShaderProgram& shaderProgram, const json jsonFile, int tileSize);
	void render();
	int takeJar();
	glm::ivec2 getPosition();

private:
	Texture spritesheet;
	ShaderProgram texProgram;
	Sprite* sprite;
	glm::ivec2 tileMapDispl, posJar;

	int id_jar, exp;
	bool taken;


};

#endif //_JAR_INCLUDE