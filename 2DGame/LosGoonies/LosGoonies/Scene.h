#ifndef _SCENE_INCLUDE
#define _SCENE_INCLUDE


#include <glm/glm.hpp>
#include "ShaderProgram.h"
#include "TileMap.h"
#include "Player.h"


// Scene contains all the entities of our game.
// It is responsible for updating and render them.


class Scene
{

public:
	Scene();
	~Scene();

	void init();
	void update(int deltaTime);
	void render();

	void initial_screen();
	void change_scene1();
	void change_scene2();
	void change_scene3();
	void change_scene4();
	void change_scene5();
	void change_instructions();
	void change_credits();
	void game_over();

private:
	void initShaders();

private:
	map<int, TileMap*> layout;
	TileMap *map;
	Player *player;
	ShaderProgram texProgram;
	float currentTime;
	glm::mat4 projection;

	bool menu, instructions, gameover, credits, bplayer;
	int scene;

};


#endif // _SCENE_INCLUDE

