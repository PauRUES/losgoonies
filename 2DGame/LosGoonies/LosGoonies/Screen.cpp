#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <glm/gtc/matrix_transform.hpp>
#include "Screen.h"

#include <json.hpp>


using namespace std;
using json = nlohmann::json;

#define INIT_ENEMY_X_TILES 2
#define INIT_ENEMY_Y_TILES 7

Screen::Screen()
{
	map = NULL;
	door = NULL; 
	key = NULL; 
	jar = NULL; 
	map_items = NULL; 
	enemy = NULL;
	tileSize = 0, bottom=0, right=0 , left = 0,right = 0;
}

Screen::Screen(const glm::ivec2& tileMapPos, ShaderProgram& shaderProgram,  int tSize, int i, int t, int b, int r, int l, int* m)
{
	tileSize = tSize; 
	id = i;
	top = t;
	bottom = b;
	right = r;
	left = l;
	map = m;
	map_items = new int[32*28]; 
	memset(&map_items[0], -1, sizeof(int) * 32*28);
	int aux = map_items[1]; 
	int aux1 = map_items[32];
	string tilesheetFile; 

	json screen_data;
	ifstream file("levels/Enemies_screen_2.json", std::ifstream::binary);
	
	file >> screen_data;

	//Vamos a leer los enemigos que tiene esta pantalla en la escena que esta
	

	//Leer los enemigos
	json enemies = screen_data["screens"][id]["enemies"]; 
	num_enemies = enemies["num_enemies"];

	if (num_enemies != 0) {
		enemy = new Enemy[num_enemies]; 

		for (int i = 0; i < num_enemies; i++) {
			json en = enemies["data"][i]; 
			enemy[i].init(i,tileMapPos, shaderProgram, en, tileSize);
		}
	}

	//Leer las puertas
	has_door = screen_data["screens"][id]["has_door"]; 
	if (has_door) {
		json door_json = screen_data["screens"][id]["door"];
		door = new Door();
		door->init(tileMapPos,door_json, glm::vec2(32, 28), shaderProgram);
		glm::ivec2 posLockDoor = door->getPositionLock();
		int x = posLockDoor.x / tileSize;
		int y = posLockDoor.y / tileSize;
		map_items[y * 32 + x] = 11;
	}


	//Leer las llaves
	has_keys = screen_data["screens"][id]["has_keys"];
	if (has_keys) {
		json key_json = screen_data["screens"][id]["keys"];
		key= new Key();
		key->init(glm::vec2(32, 28), shaderProgram,key_json,tileSize);
		glm::ivec2 posKey = key->getPosition(); 
		int x = posKey.x / tileSize;
		int y = posKey.y / tileSize;
		map_items[y * 32 + x] = 10;

	}

	//Leer los frascos 
	has_jar = screen_data["screens"][id]["has_jar"];
	if (has_jar) {
		json jar_json = screen_data["screens"][id]["jar"];
		jar = new Jar();
		jar->init(glm::vec2(32, 28), shaderProgram, jar_json, tileSize);
		glm::ivec2 posJar = jar->getPosition();
		int x = posJar.x / tileSize;
		int y = posJar.y / tileSize;
		map_items[y * 32 + x] = 12;

	}
}

void Screen::render()
{
	if (has_jar) jar->render();
	if (has_door ) door->render();
	if (has_keys) key->render(); 
	//if (has_jar) jar->render(); 
	
	for (int i = 0; i < num_enemies; i++) {
		enemy[i].render();
	}
	
}

void Screen::update_enemies(int deltaTime)
{
	glm::vec2 pos;
	for (int i = 0; i < num_enemies; i++) {

		if (!enemy[i].isDead()) {
			pos = enemy[i].getPosition();
			int x = pos.x / tileSize;
			int y = pos.y / tileSize;
			map_items[y * 32 + x]= -1;
			enemy[i].update(deltaTime);
			pos = enemy[i].getPosition();
			x = pos.x / tileSize;
			y = pos.y / tileSize;
			map_items[y * 32 + x] = i;
		}
	}
}

void Screen::attack_enemy(int id_enemy)
{

	glm::vec2 pos = enemy[id_enemy].getPosition();
	int x = pos.x / tileSize;
	int y = pos.y / tileSize;
	map_items[y * 32 + x] = -1;
	enemy[id_enemy].attacked(); 
}

void Screen::openDoor(int id_key)
{
	if (has_door) door->openDoor(id_key); 
}

/*int Screen::collisionEnemyLeft(const glm::ivec2& pos, const glm::ivec2& size)
{	
	int x, y0, y1;

	x = ((pos.x ) / tileSize)  ;
	y0 = pos.y / tileSize;
	y1 = (pos.y + size.y - 1) / tileSize;
	for (int y = y0; y <= y1; y++)
	{
		if (map_enemies[y * 32 + x] != -1)
			return map_enemies[y * 32 + x];
	}

	return -1;

}

int Screen::collisionEnemyRight(const glm::ivec2& pos, const glm::ivec2& size)
{
	int x, y0, y1;

	x = ((pos.x ) / tileSize) ;
	y0 = pos.y / tileSize;
	y1 = (pos.y + size.y - 1) / tileSize;
	for (int y = y0; y <= y1; y++)
	{
		if (map_enemies[y * 32 + x] != -1)
			return map_enemies[y * 32 + x ];
	}

	return -1;
}*/

int Screen::collisionEnemy(const glm::ivec2& pos, const glm::ivec2& size) {
	int x, y0, y1;

	x = pos.x / tileSize;
	y0 = pos.y / tileSize;
	y1 = (pos.y + size.y - 1) / tileSize;

	for (int y = y0; y <= y1; y++)
	{
		if (map_items[y * 32 + x] != -1 && map_items[y * 32 + x] != 10 && map_items[y * 32 + x] != 11 && map_items[y * 32 + x] != 12)
			return map_items[y * 32 + x];
	}

	return -1; 
}

bool Screen::collisionKey(const glm::ivec2& pos, const glm::ivec2& size)
{
	int x0, x1, y;

	x0 = pos.x / tileSize;
	x1 = (pos.x + size.x - 1) / tileSize;
	y = (pos.y + size.y - 1) / tileSize;
	for (int x = x0; x <= x1; x++)
	{
		int something = map_items[(y) * 32 + x]; 
		if (map_items[(y) * 32 + x] == 10)
			return true;
	}

	return false;
}

bool Screen::collisionJar(const glm::ivec2& pos, const glm::ivec2& size)
{
	int x0, x1, y;

	x0 = pos.x / tileSize;
	x1 = (pos.x + size.x - 1) / tileSize;
	y = (pos.y + size.y - 1) / tileSize;
	for (int x = x0; x <= x1; x++)
	{
		int something = map_items[(y) * 32 + x];
		if (map_items[(y) * 32 + x] == 12)
			return true;
	}

	return false;
}

bool Screen::collisionLock(const glm::ivec2& pos, const glm::ivec2& size)
{
	int x0, x1, y;
	if (has_door) {

		x0 = pos.x / tileSize;
		x1 = (pos.x + size.x - 1) / tileSize;
		y = (pos.y + size.y - 1) / tileSize;
		glm::ivec2 posLock = door->getPositionLock();
		for (int x = x0; x <= x1; x++)
		{
			if (x == (posLock.x / tileSize) && y == (posLock.y / tileSize))
				return true;
		}
		
	}
	

	return false;
}

int Screen::attackEnemyLeft(const glm::ivec2& pos, const glm::ivec2& size)
{
	int x, y0, y1;

	x = ((pos.x - size.x ) / tileSize) ;
	y0 = pos.y / tileSize;
	y1 = (pos.y + size.y - 1) / tileSize;
	for (int y = y0; y <= y1; y++)
	{

		if (map_items[y * 32 + x] != -1)
			return map_items[y * 32 + x];
	}

	return -1;
}

int Screen::attackEnemyRight(const glm::ivec2& pos, const glm::ivec2& size)
{
	int x, y0, y1;

	x = ((pos.x + size.x) / tileSize) ;
	y0 = pos.y / tileSize;
	y1 = (pos.y + size.y - 1) / tileSize;
	for (int y = y0; y <= y1; y++)
	{
		if (map_items[y * 32 + x] != -1)
			return map_items[y * 32 + x];
	}

	return -1;
}

int Screen::takeKey()
{
	glm::ivec2 posKey = key->getPosition();
	int x = posKey.x / tileSize;
	int y = posKey.y / tileSize;
	map_items[y * 32 + x] = -1;
	return key->takeKey(); 
}

int Screen::takeJar()
{
	glm::ivec2 posKey = jar->getPosition();
	int x = posKey.x / tileSize;
	int y = posKey.y / tileSize;
	map_items[y * 32 + x] = -1;
	return jar->takeJar();
}


bool Screen::takeKid(const glm::ivec2& pos, const glm::ivec2& size)
{
	if (has_door) {
		int x0, x1, y;

		x0 = pos.x / tileSize;
		x1 = (pos.x + size.x - 1) / tileSize;
		y = (pos.y + size.y - 1) / tileSize;
		glm::ivec2 posLock = door->getPositionKid();
		for (int x = x0; x <= x1; x++)
		{
			if (x == (posLock.x / tileSize) && y == (posLock.y / tileSize))
				return door->takeKid(); 
				
		}
	}
	return false;
}

void Screen::prepareArrays(const glm::vec2& minCoords, ShaderProgram& program)
{
}

