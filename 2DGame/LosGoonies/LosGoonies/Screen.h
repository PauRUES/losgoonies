#ifndef _SCREEN_INCLUDE
#define _SCREEN_INCLUDE

#include <glm/glm.hpp>
#include "Texture.h"
#include "Enemy.h"
#include "ShaderProgram.h"
#include <map>
#include "Door.h"
#include "Key.h"
#include "Jar.h"



class Screen {
	
public:
	Screen();
	Screen(const glm::ivec2& tileMapPos, ShaderProgram& shaderProgram, int tileSize, int id, int top, int bottom, int right, int left, int* map);
	//~Screen();
	void render(); 
	void update_enemies(int deltaTime); 
	void attack_enemy(int id_enemy);
	void openDoor(int id_key); 
	//int collisionEnemyLeft(const glm::ivec2& pos, const glm::ivec2& size);
	//int collisionEnemyRight(const glm::ivec2& pos, const glm::ivec2& size);
	int collisionEnemy(const glm::ivec2& pos, const glm::ivec2& size);
	bool collisionKey(const glm::ivec2& pos, const glm::ivec2& size);
	bool collisionJar(const glm::ivec2& pos, const glm::ivec2& size);

	bool collisionLock(const glm::ivec2& pos, const glm::ivec2& size);


	int attackEnemyLeft(const glm::ivec2& pos, const glm::ivec2& size);
	int attackEnemyRight(const glm::ivec2& pos, const glm::ivec2& size);

	int takeKey();
	int takeJar();
	bool takeKid(const glm::ivec2& pos, const glm::ivec2& size);

	void prepareArrays(const glm::vec2& minCoords, ShaderProgram& program); 


	int top, bottom, right, left, tileSize;
	int* map;
	int id; 
	int num_enemies,num_doors; 
	int* map_items; 
	bool has_door, has_keys, has_jar; 


	Door* door; 
	Key* key; 
	Jar* jar; 
	Enemy *enemy;

};

#endif // _SCENE_INCLUDE