﻿#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "TileMap.h"
#include <json.hpp>


using namespace std;
using json = nlohmann::json; 


TileMap* TileMap::createTileMap(const string& levelFile, const glm::vec2& minCoords, ShaderProgram& program, int screen, int layer)
{
	TileMap* map = new TileMap(levelFile, minCoords, program, screen, layer);

	return map;
}


TileMap::TileMap(const string& levelFile, const glm::vec2& minCoords, ShaderProgram& program, int s, int layer)
{
	if (layer != 2) {
		loadLevel2(levelFile);
		prepareArrays2(minCoords, program);
	}
	else {
		loadLevel(levelFile,minCoords,program);
		screen = s;
		prepareArrays(minCoords, program);
	}
}



TileMap::~TileMap()
{
	if(map != NULL)
		delete map;
}


void TileMap::render() 
{

	glEnable(GL_TEXTURE_2D);
	tilesheet.use();
	glBindVertexArray(vao);
	glEnableVertexAttribArray(posLocation);
	glEnableVertexAttribArray(texCoordLocation);
	glDrawArrays(GL_TRIANGLES, 0, 6 * 32 * 28);
	glDisable(GL_TEXTURE_2D);

	if (isMap) tileMaps[screen].render();
}

void TileMap::free()
{
	glDeleteBuffers(1, &vbo);
}

void TileMap::update_screen(int deltaTime)
{
	tileMaps[screen].update_enemies(deltaTime); 
}

bool TileMap::loadLevel(const string &levelFile,const glm::vec2& minCoords, ShaderProgram& program)
{

	json tile;
	string line, tilesheetFile;
	stringstream sstream;

	ifstream file(levelFile.c_str(), std::ifstream::binary);

	if (!file.is_open()) return false;

	file >> tile;

	//Tama�o del mapa
	mapSize.x = tile["width"];
	mapSize.y = tile["height"];

	//Tama�o de los tiles
	tileSize = tile["tileheight"];
	blockSize = tile["tilewidth"];

	// Path de los recursos utilizados
	tilesheetFile = tile["tilesets"][0]["source"];
	tilesheet.loadFromFile(tilesheetFile, TEXTURE_PIXEL_FORMAT_RGBA);
	tilesheet.setWrapS(GL_CLAMP_TO_EDGE);
	tilesheet.setWrapT(GL_CLAMP_TO_EDGE);
	tilesheet.setMinFilter(GL_NEAREST);
	tilesheet.setMagFilter(GL_NEAREST);

	//Tama�os de los tile sheets 
	tilesheetSize.x = tile["tilesets"][0]["tilesheetSizeX"];
	tilesheetSize.y = tile["tilesets"][0]["tilesheetSizeY"];
	tileTexSize = glm::vec2(1.f / tilesheetSize.x, 1.f / tilesheetSize.y);

	//Lectura de las layers
	screenSize.x = 32;
	screenSize.y = 28; 
	isMap = true; 
	int w = mapSize.x / 32;
	int h = mapSize.y / 28;
	int id = 0;
	
	for (int i = 0; i < h; i++) {
		
		for (int j = 0; j < w; j++) {
			
			if (tile["layers"][0]["data"][j * screenSize.x + i*screenSize.y * mapSize.x + 4* mapSize.x] != 0) {
				int* map_aux = new int[32 * 28];
				int iter = 0;
				for (int k = i * 28; k < (i + 1) * 28; k++) {
					for (int v = j * 32; v < (j + 1) * 32; v++) {
						int aux = k * mapSize.x + v; 
						int something = tile["layers"][0]["data"][k * mapSize.x + v]; 
						map_aux[iter++] = tile["layers"][0]["data"][k * mapSize.x  + v];
					}

				}
				int top , bott, right, left;

				if (i != 0) top = id - w;
				else top = -1;

				if (j != 0) left = id - 1;
				else  left = -1;

				if (i != h-1) bott = id + w;
				else bott = -1;

				if (j != w - 1) right = id + 1;
				else right = -1; 


				Screen s = Screen(minCoords, program, tileSize, id, top, bott, right, left, map_aux);
				tileMaps[s.id] = s; 
				
			}

			id++; 
		}
		
	}
	

	file.close();

	return true;
}

void TileMap::prepareArrays(const glm::vec2 &minCoords, ShaderProgram &program)
{
	int tile, nTiles = 0;
	glm::vec2 posTile, texCoordTile[2], halfTexel;
	vector<float> vertices;
	Screen s = tileMaps[screen];
	map= s.map; 
	halfTexel = glm::vec2(0.5f / tilesheet.width(), 0.5f / tilesheet.height());
	for(int j=0; j<28; j++)
	{
		for(int i=0; i<32; i++)
		{
			tile = map[j * 32 + i];
			if(tile != 0)
			{
				// Non-empty tile
				nTiles++;
				posTile = glm::vec2(minCoords.x + i * tileSize, minCoords.y + j * tileSize);
				texCoordTile[0] = glm::vec2(float((tile-1)% tilesheetSize.x) / tilesheetSize.x, float((tile-1)/ tilesheetSize.x) / tilesheetSize.y);
				texCoordTile[1] = texCoordTile[0] + tileTexSize;
				//texCoordTile[0] += halfTexel;
				texCoordTile[1] -= halfTexel;
				// First triangle
				vertices.push_back(posTile.x); vertices.push_back(posTile.y);
				vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[0].y);
				vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y);
				vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[0].y);
				vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y + blockSize);
				vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[1].y);
				// Second triangle
				vertices.push_back(posTile.x); vertices.push_back(posTile.y);
				vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[0].y);
				vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y + blockSize);
				vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[1].y);
				vertices.push_back(posTile.x); vertices.push_back(posTile.y + blockSize);
				vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[1].y);
			}
		}
	}

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, 24 * nTiles * sizeof(float), &vertices[0], GL_STATIC_DRAW);
	posLocation = program.bindVertexAttribute("position", 2, 4*sizeof(float), 0);
	texCoordLocation = program.bindVertexAttribute("texCoord", 2, 4*sizeof(float), (void *)(2*sizeof(float)));
}

bool TileMap::loadLevel2(const string& levelFile)
{

	json tile;
	string line, tilesheetFile;
	stringstream sstream;

	ifstream file(levelFile.c_str(), std::ifstream::binary);

	if (!file.is_open()) return false;

	file >> tile;

	//Tama?o del mapa
	mapSize.x = tile["width"];
	mapSize.y = tile["height"];

	//Tama?o de los tiles
	tileSize = tile["tileheight"];
	blockSize = tile["tilewidth"];

	// Path de los recursos utilizados
	tilesheetFile = tile["tilesets"][0]["source"];
	tilesheet.loadFromFile(tilesheetFile, TEXTURE_PIXEL_FORMAT_RGBA);
	tilesheet.setWrapS(GL_CLAMP_TO_EDGE);
	tilesheet.setWrapT(GL_CLAMP_TO_EDGE);
	tilesheet.setMinFilter(GL_NEAREST);
	tilesheet.setMagFilter(GL_NEAREST);

	//Tama?os de los tile sheets 
	tilesheetSize.x = tile["tilesets"][0]["tilesheetSizeX"];
	tilesheetSize.y = tile["tilesets"][0]["tilesheetSizeY"];
	tileTexSize = glm::vec2(1.f / tilesheetSize.x, 1.f / tilesheetSize.y);
	isMap = false; 
	//Lectura de las layers
	int iter = 0;
	map = new int[mapSize.x * mapSize.y];
	for (int j = 0; j < mapSize.y; j++)
	{
		for (int i = 0; i < mapSize.x; i++)
		{
			map[j * mapSize.x + i] = tile["layers"][0]["data"][iter++];
		}

	}


	file.close();

	return true;
}

void TileMap::prepareArrays2(const glm::vec2& minCoords, ShaderProgram& program)
{
	int tile, nTiles = 0;
	glm::vec2 posTile, texCoordTile[2], halfTexel;
	vector<float> vertices;

	halfTexel = glm::vec2(0.5f / tilesheet.width(), 0.5f / tilesheet.height());
	for (int j = 0; j < mapSize.y; j++)
	{
		for (int i = 0; i < mapSize.x; i++)
		{
			tile = map[j * mapSize.x + i];
			if (tile != 0)
			{
				// Non-empty tile
				nTiles++;
				posTile = glm::vec2(minCoords.x + i * tileSize, minCoords.y + j * tileSize);
				texCoordTile[0] = glm::vec2(float((tile - 1) % tilesheetSize.x) / tilesheetSize.x, float((tile - 1) / tilesheetSize.x) / tilesheetSize.y);
				texCoordTile[1] = texCoordTile[0] + tileTexSize;
				//texCoordTile[0] += halfTexel;
				texCoordTile[1] -= halfTexel;
				// First triangle
				vertices.push_back(posTile.x); vertices.push_back(posTile.y);
				vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[0].y);
				vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y);
				vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[0].y);
				vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y + blockSize);
				vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[1].y);
				// Second triangle
				vertices.push_back(posTile.x); vertices.push_back(posTile.y);
				vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[0].y);
				vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y + blockSize);
				vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[1].y);
				vertices.push_back(posTile.x); vertices.push_back(posTile.y + blockSize);
				vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[1].y);
			}
		}
	}

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, 24 * nTiles * sizeof(float), &vertices[0], GL_STATIC_DRAW);
	posLocation = program.bindVertexAttribute("position", 2, 4 * sizeof(float), 0);
	texCoordLocation = program.bindVertexAttribute("texCoord", 2, 4 * sizeof(float), (void*)(2 * sizeof(float)));
}

// Collision tests for axis aligned bounding boxes.
// Method collisionMoveDown also corrects Y coordinate if the box is
// already intersecting a tile below.

bool TileMap::collisionMoveLeft(const glm::ivec2& pos, const glm::ivec2& size) const
{
	int x, y0, y1;

	x = pos.x / tileSize;
	y0 = pos.y / tileSize;
	y1 = (pos.y + size.y - 1) / tileSize;
	for (int y = y0; y <= y1; y++)
	{
		if (map[y * screenSize.x + x] == 132 || (map[y * screenSize.x + x] >= 160 && map[y * screenSize.x + x] <= 166) || (map[y * screenSize.x + x] >= 7 && map[y * screenSize.x + x] <= 15))
			return true;
	}

	return false;
}

bool TileMap::collisionMoveRight(const glm::ivec2& pos, const glm::ivec2& size) const
{
	int x, y0, y1;

	x = (pos.x + size.x - 1) / tileSize;
	y0 = pos.y / tileSize;
	y1 = (pos.y + size.y - 1) / tileSize;
	for (int y = y0; y <= y1; y++)
	{
		if ((map[y * screenSize.x + x] >= 126 && map[y * screenSize.x + x] <= 132) || (map[y * screenSize.x + x] >= 7 && map[y * screenSize.x + x] <= 15))
			return true;
	}

	return false;
}

bool TileMap::collisionMoveDown(const glm::ivec2& pos, const glm::ivec2& size, int* posY) const
{
	int x0, x1, y;

	x0 = pos.x / tileSize;
	x1 = (pos.x + size.x - 1) / tileSize;
	y = (pos.y + size.y - 1) / tileSize;
	for (int x = x0; x <= x1; x++)
	{
		if ((map[y * screenSize.x + x] >= 1 && map[y * screenSize.x + x] <= 4) || map[y * screenSize.x + x] == 6 || map[y * screenSize.x + x] == 42 || map[y * screenSize.x + x] == 38|| map[y * screenSize.x + x] == 40 || map[y * screenSize.x + x] == 38 || map[y * screenSize.x + x] == 40 || (map[y * screenSize.x + x] >= 119 && map[y * screenSize.x + x] <= 123) || map[y * screenSize.x + x] == 125 || map[y * screenSize.x + x] == 126 || map[y * screenSize.x + x] == 159 || map[y * screenSize.x + x] == 160 || map[y * screenSize.x + x] == 138 || map[y * screenSize.x + x] == 172 || map[y * screenSize.x + x] == 131 || map[y * screenSize.x + x] == 165 || map[y * screenSize.x + x] == 174 || map[y * screenSize.x + x] == 140) /*si el tile es diferente a negro tene colision por debajo*/
		{
			if (*posY - tileSize * y + size.y <= 4)
			{
				*posY = tileSize * y - size.y;
				return true;
			}
		}
	}

	return false;
}

bool TileMap::collisionMoveUp(const glm::ivec2& pos, const glm::ivec2& size, int* posY) const
{
	int x, y;

	x = (pos.x / tileSize) + 1;
	y = (pos.y) / tileSize;

	if ((map[y * screenSize.x + x] >= 126 && map[y * screenSize.x + x] <= 132)  || (map[y * screenSize.x + x] >= 160 && map[y * screenSize.x + x] <= 166) || (map[y * screenSize.x + x] >= 10 && map[y * screenSize.x + x] <= 16) || map[y * screenSize.x + x] == 22 ||	map[y * screenSize.x + x] == 23) //si el la posicion del player se encuentra con un techo no puede saltar mas
	{
		return true;
	}
	return false;
}

bool TileMap::lianaup(const glm::ivec2& pos, const glm::ivec2& size, int* posY) const
{
	int x0, x1, y, y2, y0;
	x0 = (pos.x / tileSize)+1;
	y0 = (pos.y / tileSize)+1;
	y = (pos.y / tileSize) + 2;
	int something = map[y0 * screenSize.x + x0];
	if (something == 2 || something == 21 || something == 20 || (something == 1 && map[y * screenSize.x + x0] == 20))
		return true;


	return false;
}

bool TileMap::lianadown(const glm::ivec2& pos, const glm::ivec2& size, int* posY) const
{
	int x0, x1, y, y2, y0;
	x0 = (pos.x / tileSize) + 1;
	y0 = (pos.y / tileSize) + 2;
	int something = map[y0 * screenSize.x + x0];
	y = (pos.y / tileSize) + 3;

	if (something == 2 || something == 21 || something == 20 || (something == 1 && map[y * screenSize.x + x0] == 20))
		return true;
	return false;
}

/*int TileMap::collisionEnemyLeft(const glm::ivec2& pos, const glm::ivec2& size)
{
	return tileMaps[screen].collisionEnemyLeft(pos,size);
}

int TileMap::collisionEnemyRight(const glm::ivec2& pos, const glm::ivec2& size)
{
	return tileMaps[screen].collisionEnemyRight(pos,size);
}*/

int TileMap::collisionEnemy(const glm::ivec2& pos, const glm::ivec2& size)
{
	return tileMaps[screen].collisionEnemy(pos, size);
}

bool TileMap::collisionKey(const glm::ivec2& pos, const glm::ivec2& size)
{
	return  tileMaps[screen].collisionKey(pos, size);
}

bool TileMap::collisionJar(const glm::ivec2& pos, const glm::ivec2& size)
{
	return  tileMaps[screen].collisionJar(pos, size);
}

bool TileMap::collisionLock(const glm::ivec2& pos, const glm::ivec2& size)
{
	return tileMaps[screen].collisionLock(pos, size);
}

bool TileMap::collisionKid(const glm::ivec2& pos, const glm::ivec2& size)
{
	return tileMaps[screen].takeKid(pos, size);
}

int TileMap::attackEnemyLeft(const glm::ivec2& pos, const glm::ivec2& size)
{
	return tileMaps[screen].attackEnemyLeft(pos, size);
}

int TileMap::attackEnemyRight(const glm::ivec2& pos, const glm::ivec2& size)
{
	return tileMaps[screen].attackEnemyRight(pos, size);
}

int TileMap::takeKey()
{
	return tileMaps[screen].takeKey(); 
}

int TileMap::takeJar()
{
	return tileMaps[screen].takeJar();
}

void TileMap::attack_enemy(int id_enemy)
{
	tileMaps[screen].attack_enemy(id_enemy); 
}

void TileMap::openDoor(int id_key)
{
	tileMaps[screen].openDoor(id_key);
}

void TileMap::updateBar(int lp, int times, int bar, ShaderProgram& program) {
	int y, x;
	if (bar == 0)
		y = 2;
	else
		y = 3;
	x = lp / 9 + 1;
	if (lp / 9 != (lp + times) / 9 && (lp + times) != 81) {
		map[y * mapSize.x + x + 6] = 9;
		map[y * mapSize.x + x + 5] = (lp % 9);
	}
	else if (lp%9 == 0)
		map[y * mapSize.x + x + 5] = 9;
	else
		map[y * mapSize.x + x + 5] = (lp % 9);

	prepareArrays2(glm::vec2(32, 28), program); 
}


int TileMap::endScreen(const glm::ivec2& pos)
{
	if (pos.x /tileSize== screenSize.x-1) {
		return tileMaps[screen].right; 
	}
	else if (pos.x/ tileSize == 0) {
		return tileMaps[screen].left;
	}
	else if (pos.y/tileSize == screenSize.y-1) {
		return tileMaps[screen].bottom;
	}
	else if ((pos.y/tileSize) -4== 0) {
		return tileMaps[screen].top;
	}
	else return -1; 

}

glm::ivec2& TileMap::changeScreen(int id, const glm::vec2& minCoords, const glm::vec2& actualPos, ShaderProgram& program)
{
	Screen s = tileMaps[screen]; 
	glm::ivec2 newpos; 

	if (id == s.top) newpos = glm::vec2(actualPos.x, (screenSize.y-6) * tileSize);
	else if (id == s.bottom) newpos = glm::vec2(actualPos.x, 6*tileSize);
	else if (id == s.left) newpos = glm::vec2((screenSize.x -1)* tileSize, actualPos.y);
	else  newpos = glm::vec2(tileSize, actualPos.y);

	screen = id;
	prepareArrays(minCoords, program); 

	return newpos; 
}



bool TileMap::puerta(const glm::ivec2& pos, const glm::ivec2& size, int* posY) const {

	int x0, x1, x2, y;

	x0 = pos.x / tileSize;
	x1 = (pos.x + size.x - 1) / tileSize;
	x2 = (pos.x - size.x + 1) / tileSize;
	y = (pos.y + size.y*2 - 1) / tileSize;
	for (int x = x0; x <= x1; x++)
	{
		if (map[y * screenSize.x + x] == 120) /*se encuentra con una puerta*/
		{
			if (*posY - tileSize * y + size.y <= 4)
			{
				*posY = tileSize * y - size.y;
				return true;
			}
		}
	}
	for (int x = x2; x <= x0; x++)
	{
		if (map[y * screenSize.x + x] == 119) /*se encuentra con una puerta*/
		{
			if (*posY - tileSize * y + size.y <= 4)
			{
				*posY = tileSize * y - size.y;
				return true;
			}
		}
	}

	return false;
}































