#ifndef _TILE_MAP_INCLUDE
#define _TILE_MAP_INCLUDE


#include <glm/glm.hpp>
#include "Texture.h"
#include "ShaderProgram.h"
#include <map>
#include "Screen.h"
#include "../../../libs/json/json.hpp"


// Class Tilemap is capable of loading a tile map from a text file in a very
// simple format (see level01.txt for an example). With this information
// it builds a single VBO that contains all tiles. As a result the render
// method draws the whole map independently of what is visible.

struct Screen_map {
	int* map;
	int top, left, right, bottom; 
};

class TileMap
{

public:
	// Tile maps can only be created inside an OpenGL context
	static TileMap* createTileMap(const string& levelFile, const glm::vec2& minCoords, ShaderProgram& program, int screen, int layer);
	TileMap(const string& levelFile, const glm::vec2& minCoords, ShaderProgram& program, int screen, int layer);
	~TileMap();

	void render();
	void free();
	void update_screen(int deltaTime); 
	int layer;
	
	int getTileSize() const { return tileSize; }

	bool collisionMoveLeft(const glm::ivec2 &pos, const glm::ivec2 &size) const;
	bool collisionMoveRight(const glm::ivec2 &pos, const glm::ivec2 &size) const;
	bool collisionMoveDown(const glm::ivec2 &pos, const glm::ivec2 &size, int *posY) const;
	bool collisionMoveUp(const glm::ivec2& pos, const glm::ivec2& size, int* posY) const;
	bool puerta(const glm::ivec2& pos, const glm::ivec2& size, int* posY) const;
	bool lianaup(const glm::ivec2& pos, const glm::ivec2& size, int* posY) const;
	bool lianadown(const glm::ivec2& pos, const glm::ivec2& size, int* posY) const;
	//int collisionEnemyLeft(const glm::ivec2& pos, const glm::ivec2& size);
	//int collisionEnemyRight(const glm::ivec2& pos, const glm::ivec2& size);

	int collisionEnemy(const glm::ivec2& pos, const glm::ivec2& size);
	bool collisionKey(const glm::ivec2& pos, const glm::ivec2& size);
	bool collisionJar(const glm::ivec2& pos, const glm::ivec2& size);

	bool collisionLock(const glm::ivec2& pos, const glm::ivec2& size);
	bool collisionKid(const glm::ivec2& pos, const glm::ivec2& size);

	int attackEnemyLeft(const glm::ivec2& pos, const glm::ivec2& size);
	int attackEnemyRight(const glm::ivec2& pos, const glm::ivec2& size);
	int takeKey();
	int takeJar();


	void attack_enemy(int id_enemy); 
	void openDoor(int id_key);
	void updateBar(int lp, int times, int bar, ShaderProgram& program);

	int endScreen(const glm::ivec2& pos);

	glm::ivec2& changeScreen(int id, const glm::vec2& minCoords, const glm::vec2& actualPos, ShaderProgram& program);

private:
	bool loadLevel(const string &levelFileconst, const glm::vec2& minCoords, ShaderProgram& program);
	void prepareArrays(const glm::vec2 &minCoords, ShaderProgram &program);
	bool loadLevel2(const string& levelFile);
	void prepareArrays2(const glm::vec2& minCoords, ShaderProgram& program);

private:
	GLuint vao;
	GLuint vbo;
	GLint posLocation, texCoordLocation;
	glm::ivec2 position, mapSize, screenSize,  tilesheetSize;
	int tileSize, blockSize;
	Texture tilesheet;
	glm::vec2 tileTexSize;
	int *map;
	int screen; 

	

	bool isMap; 
	
	std::map<int, Screen> tileMaps;

};


#endif // _TILE_MAP_INCLUDE


